import {Attachment, login, MastoClient, Status} from "masto";
import {ConfigTemplate} from "../config/config-template";
import fs from "node:fs";
import {MastodonStatus} from "../model/mastodon-status.model";
import {MastodonFile} from "../model/mastodon-file";

export interface IMastodonRepository {
  LogIn(): Promise<MastoClient>;

  UploadFileToMastodon(mastodonFile: MastodonFile): Promise<Attachment>;

  CreateStatus(statusModel: MastodonStatus): Promise<Status>;
}

export class MastodonRepository implements IMastodonRepository {
 private _client: MastoClient;

 public async CreateStatus(statusModel: MastodonStatus): Promise<Status> {
   await this.LogIn();
   return await this._client.statuses.create({
     status: statusModel.status,
     visibility: statusModel.visibility,
     mediaIds: statusModel.mediaIds
   });

 }

  public async UploadFileToMastodon(mastodonFile: MastodonFile): Promise<Attachment> {
  await this.LogIn();
  try {
    const file = fs.createReadStream(mastodonFile.fileName)
    return await this._client.mediaAttachments.create({
      file:  file,
      description: mastodonFile.description
    });
  }catch(err) {
    console.error(err);
    throw err;
  }

  }
  public async LogIn(): Promise<MastoClient>{
   if(this._client) {
     return this._client;
   }
   this._client = await login({
     url: this._config.MASTODON_BASE_URL,
     accessToken: this._config.MASTODON_BEARER_TOKEN
   });
   return this._client;
 }
  constructor(private readonly _config: ConfigTemplate) {
  }
}
