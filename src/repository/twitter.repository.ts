import {Client} from "twitter-api-sdk";
import {findUserByUsername, TwitterPaginatedResponse, TwitterResponse, usersIdTweets} from "twitter-api-sdk/dist/types";
import {ConfigTemplate} from "../config/config-template";

export interface ITwitterRepository {
  GetUserDataByUserName(username: string): Promise<TwitterResponse<findUserByUsername>>;
  GetTweetsByUserId(userId: string, startTime: Date): Promise<TwitterPaginatedResponse<TwitterResponse<usersIdTweets>>>;
}

export class TwitterRepository implements TwitterRepository {

  private _client: Client;

public async GetTweetsByUserId(userId: string, startTime: Date): Promise<TwitterPaginatedResponse<TwitterResponse<usersIdTweets>>> {
  return this._client.tweets.usersIdTweets(
    userId,
    {
      start_time: startTime.toISOString(),
      "expansions": ["attachments.media_keys"],
      "media.fields": ["media_key", "url", "alt_text", "preview_image_url", "type", "height", "width", "duration_ms", "variants"],
      "tweet.fields": ["source", "text", "id", "entities", "attachments"]
    }
  )
}

  public async GetUserDataByUserName(username: string): Promise<TwitterResponse<findUserByUsername>> {
    return await this._client.users.findUserByUsername(username);
  }

  constructor(private _configuration: ConfigTemplate) {
  this._client = new Client(this._configuration.TWITTER_BEARER_TOKEN);
  }
}
