import {StatusVisibility} from "masto";

export class MastodonStatus {
 status: string;
 visibility: StatusVisibility;
 mediaIds?: Array<string>;
}
