export class TwitterMediaVariant {
  public bit_rate: number | undefined;
  public content_type: string | undefined;
  public url: string | undefined;
}
