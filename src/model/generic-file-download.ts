import {UrlAndFilename} from "./url-and-filename";

export class GenericFileDownload {
  id: string;
  content: string;
  urlAndFileName: Array<UrlAndFilename>;
}
