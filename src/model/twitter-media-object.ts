import {TwitterMediaVariant} from "./twitter-media-variant";

export class TwitterMediaObject {
  public media_key: string | undefined;
  public type: string | undefined;
  public url: string | undefined;
  public duration_ms: number | undefined;
  public height: number | undefined;
  public preview_image_url: string | undefined;
  public width: string | undefined;
  public alt_text: string | undefined;
  public variants: Array<TwitterMediaVariant> | undefined;
}
