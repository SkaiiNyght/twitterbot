import {Attachment} from "masto";

export class UrlAndFilename {
  url: string;
  filename: string;
  mastodonAttachment: Attachment;
  description: string;
}
