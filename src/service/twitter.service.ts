import {ConfigTemplate} from "../config/config-template";
import {findUserByUsername, TwitterResponse, usersIdTweets} from "twitter-api-sdk/dist/types";
import {ITwitterRepository, TwitterRepository} from "../repository/twitter.repository";
import {TwitterMediaVariant} from "../model/twitter-media-variant";
import {GenericFileDownload} from "../model/generic-file-download";
import {UrlAndFilename} from "../model/url-and-filename";

export class TwitterService {
  /**
   * Self building configuration template used to retrieve application configurations
   * @private
   */
  private readonly _configuration: ConfigTemplate;

  /**
   * Repository layer that does the actual communication with twitter
   * @private
   */
  private _repository: ITwitterRepository;

  /**
   * Retrieves the highest bit-rate version of twitter media variants
   * @param mediaVariants
   * @constructor
   * @private
   */
  private GetHighestBitRateUrl(mediaVariants: Array<TwitterMediaVariant>): string {
    let highestBitRate = -1;
    let downloadUrl: string;
    for(const variant of mediaVariants) {
      if(highestBitRate < variant.bit_rate) {
        downloadUrl = variant.url;
      }
    }
    return downloadUrl;
  }

  /**
   * Returns a map of <TweetId, Array<DownloadUrl>> for a given set of tweets
   * @param allTweets
   * @constructor
   */
  public GetValidDownloadUrlForTweetMedia(allTweets: TwitterResponse<usersIdTweets>): Array<GenericFileDownload> {
    const genericDownloads = new Array<GenericFileDownload>;
    if(allTweets.data) {
      for (const tweet of allTweets.data) {
        if(tweet.attachments?.media_keys?.length > 0) {
          const mediaObjectsForTweet =
            allTweets.includes.media.filter(x =>
              tweet.attachments.media_keys.findIndex(y => x.media_key === y) > -1)
          const urlAndFileNames = new Array<UrlAndFilename>();
          for(const mediaObject of mediaObjectsForTweet) {
            let urlAndFileName = new UrlAndFilename();
            switch (mediaObject.type) {
              case 'video':
              case 'animated_gif':
                // @ts-ignore
                urlAndFileName.url = this.GetHighestBitRateUrl(mediaObject.variants);
                break;
              case 'photo':
                // @ts-ignore
                urlAndFileName.url = mediaObject.url;
                break;
            }
            urlAndFileName.description = mediaObject.media_key;
            urlAndFileNames.push(urlAndFileName)
          }
          genericDownloads.push({id: tweet.id, urlAndFileName: urlAndFileNames, content: tweet.text})
        }
        else {
          genericDownloads.push({id: tweet.id, urlAndFileName: [], content: tweet.text});
        }

      }
    }

    return genericDownloads;
  }

  /**
   * Gets all the tweets by a particular user id in a given time period which is configured in the configuration file
   * @param userId
   * @constructor
   */
  public async GetTweetsByUserId(userId: string): Promise<TwitterResponse<usersIdTweets>> {
    if(!userId) {
      throw new Error('userId must have a value')
    }
    if(userId.length === 0) {
      throw new Error('userId must be longer than 0 characters')
    }
    try{
      return this._repository.GetTweetsByUserId(userId, this.GenerateDesiredStartDate())
    }catch(err) {
      console.error(err);
      throw new Error(`Error when attempting to GetTweetsByUserId ${userId}`);
    }
  }

  /**
   * Get user data based on username
   * @param username
   * @constructor
   */
  public async GetUserDataByUserName(username: string): Promise<TwitterResponse<findUserByUsername>> {
    if(!username) {
      throw new Error('username must have a value')
    }
    if(username.length === 0) {
      throw new Error('username must be longer than 0 characters')
    }
    try{
      return this._repository.GetUserDataByUserName(username);
    } catch(err) {
      console.error(err);
      throw new Error(`Error when attempting to get user info for username ${username}`)
    }
  }

  /**
   * Gets a valid start date, dependent on a configuration value to determine the number of minutes to check
   * @constructor
   * @private
   */
  private GenerateDesiredStartDate(): Date {
    const MS_PER_MINUTE = 60000;
    const NUMBER_OF_MINUTES = +this._configuration.TWITTER_NUMBER_OF_MINUTES_TO_CHECK;
    return new Date(new Date().valueOf() - (NUMBER_OF_MINUTES * MS_PER_MINUTE));
  }

  constructor() {
    this._configuration = new ConfigTemplate();
    this._repository = new TwitterRepository(this._configuration);
  }
}
