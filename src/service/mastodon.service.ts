import {ConfigTemplate} from "../config/config-template";
import {IMastodonRepository, MastodonRepository} from "../repository/mastodon.repository";
import {GenericFileDownload} from "../model/generic-file-download";
import {MastodonStatus} from "../model/mastodon-status.model";

export class MastodonService {
  private readonly _configuration: ConfigTemplate;
  private readonly _repository: IMastodonRepository;

  public async UploadFilesToMastodon(genericFileDownloads: Array<GenericFileDownload>): Promise<Array<GenericFileDownload>> {
    for(const genericFile of genericFileDownloads) {
      if(genericFile.urlAndFileName) {
        for(const file of genericFile.urlAndFileName) {
          file.mastodonAttachment = await this._repository.UploadFileToMastodon({fileName: file.filename, description: file.description})
        }
      }

    }
    return genericFileDownloads;
  }

  public async CreateStatuses(mastodonStatuses: Array<MastodonStatus>) {
      for(const status of mastodonStatuses) {
        await this._repository.CreateStatus(status);
      }
  }

  constructor() {
    this._configuration = new ConfigTemplate();
    this._repository = new MastodonRepository(this._configuration);
  }
}
