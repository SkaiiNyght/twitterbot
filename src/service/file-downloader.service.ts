import path from "node:path";
import {GenericFileDownload} from "../model/generic-file-download";
const fs = require('fs');
const Axios = require('axios')
export class FileDownloaderService {
  private _shimPath: string;
  public CleanUpDirectories(): void {
    if (fs.existsSync(this._shimPath)) {
      fs.readdir(this._shimPath, (err: any, files: any) => {
        if (err) throw err;
        for (const file of files) {
          fs.unlink(path.join(this._shimPath, file), (err: any) => {
            if (err) throw err;
          })
        }
      })
    }
  }
  public async DownloadFiles(genericFileDownloads: Array<GenericFileDownload>): Promise<Array<GenericFileDownload>> {
    for(const genericFile of genericFileDownloads) {
      for(const urlAndFileName of genericFile.urlAndFileName) {
        urlAndFileName.filename = this.GetGoodFileName(urlAndFileName.url);
        await this.DownloadFile(urlAndFileName.url, urlAndFileName.filename);
      }
    }
  return genericFileDownloads
  }
  private BuildFilePath(shim: string): void {
    this._shimPath = path.join(__dirname, '../', `attachments/${shim}`);
  }
  private EnsureFolderExists() {
    if(!fs.existsSync(this._shimPath)) {
      fs.mkdirSync(this._shimPath, {recursive: true});
    }
  }
  private GetGoodFileName(url: string): string {
    // Combine base path and last bit of url, skipping any query parameters
    return path.join(`${this._shimPath}`, url.split('/').pop()).split('?')[0];
  }
  private async DownloadFile(url: string, destinationPath: string) {
    const response = await Axios({
      url,
      method: 'GET',
      responseType: 'stream'
    });

    return new Promise((resolve, reject) => {
      response.data.pipe(fs.createWriteStream(destinationPath))
        .on('error', reject)
        .once('close', () => resolve(destinationPath))
    });
  }

  constructor(private shimInstantiation: string) {
    this.BuildFilePath(shimInstantiation);
    this.EnsureFolderExists();
  }
}
