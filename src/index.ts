import {TwitterService} from "./service/twitter.service";
import {MastodonService} from "./service/mastodon.service";
import {FileDownloaderService} from "./service/file-downloader.service";

class Main {
  private _twitterService: TwitterService;
  private _mastodonService: MastodonService;
  private _fileDownloader: FileDownloaderService;
  private _desiredTwitterUser: string;

  private ReadArguments(): void {
    const args = process.argv.slice(2);
    if(args.length > 0) {
      this._desiredTwitterUser = args[0];
    } else {
      this._desiredTwitterUser = 'Lions';
    }
  }
  private InitializeServices(): void {
    this._twitterService = new TwitterService();
    this._mastodonService = new MastodonService();
    this._fileDownloader = new FileDownloaderService(this._desiredTwitterUser);
  }
  public async Run() {
    this.ReadArguments();
    this.InitializeServices();
    const userData = await this._twitterService.GetUserDataByUserName(this._desiredTwitterUser);
      const allTweets = await this._twitterService.GetTweetsByUserId(userData.data.id);
      const genericFileDownloads = await
        this._mastodonService.UploadFilesToMastodon(
          await this._fileDownloader.DownloadFiles(
            this._twitterService.GetValidDownloadUrlForTweetMedia(allTweets)
        ));
      await this._mastodonService.CreateStatuses(
        genericFileDownloads.map(genericFileDownload => {
          return {
            status: genericFileDownload.content,
            visibility: "public",
            mediaIds: genericFileDownload.urlAndFileName.map(item => item.mastodonAttachment.id)
          }
        })
      );
      this._fileDownloader.CleanUpDirectories();
  }
}
new Main().Run();
