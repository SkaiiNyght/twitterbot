import fs from "fs";
import path from "node:path";

export class ConfigTemplate {
  MASTODON_CLIENT_ID: string | null = null;
  MASTODON_CLIENT_SECRET: string | null = null;
  MASTODON_BEARER_TOKEN: string | null = null;
  MASTODON_REDIRECT_URI: string | null = null;
  MASTODON_BASE_URL: string | null = null;
  TWITTER_API_SECRET: string | null = null;
  TWITTER_API_ID: string | null = null;
  TWITTER_BEARER_TOKEN: string | null = null;
  TWITTER_NUMBER_OF_MINUTES_TO_CHECK: number | null = null;

  constructor() {
    if(fs.existsSync(path.join(__dirname, 'config.json'))) {
      const config = JSON.parse(fs.readFileSync(path.join(__dirname, '../', 'config/config.json'), 'utf-8'));
      try {
        this.MASTODON_BEARER_TOKEN = config.MASTODON_BEARER_TOKEN;
      } catch (err) {
        console.error(err);
        throw new Error("No MASTODON_BEARER_TOKEN in the config.json file");
      }
      try {
        this.MASTODON_CLIENT_ID = config.MASTODON_CLIENT_ID;
      }catch (err) {
        console.error(err);
        throw new Error("No MASTODON_CLIENT_ID in the config.json file");
      }
      try {
        this.TWITTER_API_ID = config.TWITTER_API_ID;
      }catch(err) {
        console.error(err);
        throw new Error("No TWITTER_API_ID in the config.json file");
      }
      try {
        this.TWITTER_BEARER_TOKEN = config.TWITTER_BEARER_TOKEN;
      }catch (err) {
       console.error(err);
       throw new Error("No TWITTER_BEARER_TOKEN in the config.json file");
      }
      try {
        this.MASTODON_CLIENT_SECRET = config.MASTODON_CLIENT_SECRET;
      } catch(err) {
        console.error(err);
        throw new Error("No MASTODON_CLIENT_SECRET in the config.json file");
      }
      try{
        this.MASTODON_REDIRECT_URI = config.MASTODON_REDIRECT_URI;
      } catch(err) {
        console.error(err);
        throw new Error("No MASTODON_REDIRECT_URI in the config.json file");
      }
      try{
        this.MASTODON_BASE_URL = config.MASTODON_BASE_URL;
      }catch(err) {
        console.error(err);
        throw new Error("No MASTODON_BASE_URL in the config.json file");
      }
      try {
        this.TWITTER_API_SECRET = config.TWITTER_API_SECRET;
      } catch(err) {
        console.error(err);
        throw new Error("No TWITTER_API_SECRET in the config.json file")
      }
      try {
        this.TWITTER_NUMBER_OF_MINUTES_TO_CHECK = +config.TWITTER_NUMBER_OF_MINUTES_TO_CHECK;
      } catch (err) {
        console.error(err);
        throw new Error("No TWITTER_NUMBER_OF_MINUTES_TO_CHECK in the config.json file");
      }
    } else {
      throw new Error("There is no config.json file in the config directory");
    }

  }

}
